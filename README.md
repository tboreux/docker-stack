![Cover](_assets/cover.png)

# 🐳 Docker Stack

This repository is the list of my Docker `compose.yaml` files.

Each folder is a stack of applications grouped by theme.

The folder `_assets/` is used to host the logo and cover of this repository.

I wrote some particular files at the root of this repository:

- [`_healthchecks.yaml`](_healthchecks.yaml): the documentation to the healthchecks used for my `compose.yaml` files.
- [`_template.yaml`](_template.yaml): the template used for all of my `compose.yaml` files ;

## Applications

### 01.docker

1. [Cloudflare-DDNS](https://github.com/timothymiller/cloudflare-ddns)
2. [DNSSync](https://github.com/marc1307/tailscale-cloudflare-dnssync)
3. [Diun](https://github.com/crazy-max/diun)
4. [Portainer](https://github.com/portainer/portainer)
5. [NGINX Proxy Manager](https://github.com/NginxProxyManager/nginx-proxy-manager)

### 02.documents

1. [DocuSeal](https://github.com/docusealco/docuseal)
2. [Drawio](https://github.com/jgraph/docker-drawio)
3. [Paperless-NGX](https://github.com/paperless-ngx/paperless-ngx)
4. [Reactive Resume](https://github.com/AmruthPillai/Reactive-Resume)
5. [Stirling-PDF](https://github.com/Stirling-Tools/Stirling-PDF)

### 03.download

1. [Metube](https://github.com/alexta69/metube)
2. [PicoShare](https://github.com/mtlynch/picoshare)
3. [Transmission](https://github.com/transmission/transmission)

### 04.it

1. [Gitea](https://docs.gitea.com/installation/install-with-docker)
2. [OpenVSCode Server](https://github.com/gitpod-io/openvscode-server)
3. [Matomo](https://github.com/matomo-org/docker)
4. [Snippet Box](https://github.com/pawelmalak/snippet-box)
5. [Speedtest Tracker](https://github.com/alexjustesen/speedtest-tracker)

### 05.life

1. [Invidious](https://github.com/iv-org/invidious)
2. [Mealie](https://github.com/mealie-recipes/mealie)
3. [Memos](https://github.com/usememos/memos)
4. [RedLib](https://github.com/redlib-org/redlib)
5. [Workout Tracker](https://github.com/jovandeginste/workout-tracker)

### 06.management

1. [Firefly III](https://github.com/firefly-iii/firefly-iii)
2. [Flame](https://github.com/pawelmalak/flame)
3. [Homepage](https://github.com/gethomepage/homepage)
4. [Planka](https://github.com/plankanban/planka)
5. [Vikunja](https://vikunja.io)

### 07.media

1. [Audiobookshelf](https://github.com/advplyr/audiobookshelf)
2. [Calibre-Web](https://github.com/janeczku/calibre-web)
3. [Immich](https://github.com/immich-app/immich)
4. [Navidrome](https://github.com/navidrome/navidrome)
5. [Peertube](https://github.com/Chocobozzz/PeerTube/)

### 08.news

1. [Bookstack](https://github.com/BookStackApp/BookStack)
2. [FreshRSS](https://github.com/FreshRSS/FreshRSS)
3. [Linkding](https://github.com/sissbruecker/linkding)
4. [Shaarli](https://github.com/shaarli/Shaarli)
5. [Wallabag](https://github.com/wallabag/wallabag)

### 09.security

1. [2FAuth](https://github.com/Bubka/2FAuth)
2. [SearXNG](https://github.com/searxng/searxng-docker)
3. [Uptime Kuma](https://github.com/louislam/uptime-kuma)
4. [Vaultwarden](https://github.com/dani-garcia/vaultwarden)

## Usage

1. First, make sure that you have `docker` and `docker compose` installed on your machine ;
2. Then, clone this repository ;
3. Create and edit the `config.yaml` or `.env` file as asked from the application documentation ;
4. Finally, inside your desired application folder, `up` it.

```bash
git clone git@gitlab.com:tboreux/docker-stack.git
cd docker-stack/01.docker/
docker compose up -d
```

## License

[![MIT License](https://img.shields.io/badge/License-MIT-4FCA50?style=flat-square)](https://choosealicense.com/licenses/mit/)
