name: 'security'

services:

#########
# 2FAUTH
#########
    2fauth_app:
        container_name: 2fauth_app
        image: 2fauth/2fauth:latest
        restart: unless-stopped
        ports:
            - 8002:8000/tcp
        volumes:
            - 2fauth_app-2fauth:/2fauth
        env_file: ./env/2fauth.env
        healthcheck:
            test: wget --quiet --tries=1 --spider --no-check-certificate http://127.0.0.1:8000 || exit 1
            interval: 15s
            timeout: 10s
            retries: 5

############
# GUACAMOLE
############
    guacamole_daemon:
        container_name: guacamole_daemon
        image: guacamole/guacd
        restart: unless-stopped
        volumes:
            - guacamole_daemon-drive:/drive:rw
            - guacamole_daemon-record:/record:rw
        healthcheck:
            test: ping -c 1 localhost > /dev/null || exit 1
            interval: 15s
            timeout: 10s
            retries: 5

    guacamole_db:
        container_name: guacamole_db
        image: postgres:latest
        restart: unless-stopped
        volumes:
            - ./volumes/guacamole_db/init:/docker-entrypoint-initdb.d:z
            - guacamole_db-data:/var/lib/postgresql/data:Z
        env_file: ./env/guacamole.env
        healthcheck:
            test: ["CMD-SHELL", "pg_isready -U $$POSTGRES_USER -d $$POSTGRES_DB"]
            interval: 15s
            timeout: 10s
            retries: 5

    guacamole_app:
        container_name: guacamole_app
        image: guacamole/guacamole:latest
        restart: unless-stopped
        depends_on:
            guacamole_db:
                condition: service_healthy
            guacamole_daemon:
                condition: service_healthy
        links:
            - guacamole_daemon
        ports:
            - 8094:8080/tcp 
        expose:
            - 8080
        env_file: ./env/guacamole.env
        healthcheck:
            test: curl -f -k http://127.0.0.1:8080/guacamole || exit 1
            interval: 15s
            timeout: 10s
            retries: 5

##########
# SEARXNG
##########
    searxng_app:
        container_name: searxng_app
        image: searxng/searxng:latest
        hostname: searxng
        restart: unless-stopped
        ports:
            - 8088:8080
        volumes:
            - searxng_app-searxng:/etc/searxng:rw
        environment:
            - SEARXNG_BASE_URL=https://search.boreux.work
        cap_drop:
            - ALL
        cap_add:
            - CHOWN
            - SETGID
            - SETUID
        logging:
            driver: "json-file"
            options:
                max-size: "1m"
                max-file: "1"
        healthcheck:
            test: ping -c 1 localhost > /dev/null || exit 1
            interval: 15s
            timeout: 10s
            retries: 5

    searxng_redis:
        container_name: searxng_redis
        image: valkey/valkey:latest
        hostname: redis
        command: valkey-server --save 30 1 --loglevel warning
        restart: unless-stopped
        volumes:
            - searxng_redis-data:/data
        cap_drop:
            - ALL
        cap_add:
            - SETGID
            - SETUID
            - DAC_OVERRIDE
        logging:
            driver: "json-file"
            options:
                max-size: "1m"
                max-file: "1"
        healthcheck:
            test: ["CMD", "redis-cli","ping"]
            interval: 15s
            timeout: 10s
            retries: 5

###############
# UPTIME KUMA
###############
    uptime_app:
        container_name: uptime_app
        image: louislam/uptime-kuma:latest
        restart: unless-stopped
        ports:
            - 3007:3001
        volumes:
            - uptime_app-data:/app/data
            - /var/run/docker.sock:/var/run/docker.sock

##############
# VAULTWARDEN
##############
    vaultwarden_app:
        container_name: vaultwarden_app
        image: vaultwarden/server:latest
        hostname: vaultwarden
        restart: unless-stopped
        dns:
            - 1.1.1.1
        ports:
            - 8091:80
        volumes:
            - vaultwarden_app-data:/data
            - vaultwarden_app-logs:/data/logs
        env_file: ./env/vaultwarden.env

volumes:
    2fauth_app-2fauth:
        name: 2fauth_app-2fauth
    guacamole_daemon-drive:
        name: guacamole_daemon-drive
    guacamole_daemon-record:
        name: guacamole_daemon-record
    guacamole_db-data:
        name: guacamole_db-data
    searxng_app-searxng:
        name: searxng_app-searxng
    searxng_redis-data:
        name: searxng_redis-data
    uptime_app-data:
        name: uptime_app-data
    vaultwarden_app-data:
        name: vaultwarden_app-data
    vaultwarden_app-logs:
        name: vaultwarden_app-logs